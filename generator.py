#!/usr/bin/env python



from PySide2.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
        QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
        QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
        QVBoxLayout, QWidget)


class WidgetGallery(QDialog):
    def __init__(self, parent=None):
        super(WidgetGallery, self).__init__(parent)

        self.originalPalette = QApplication.palette()

        self.createTopLeftGroupBox()
        self.createTopRightGroupBox()
        self.createBottomLeftTabWidget()
        self.createBottomRightGroupBox()

        mainLayout = QGridLayout()
        mainLayout.addWidget(self.topLeftGroupBox, 0, 0)
        mainLayout.addWidget(self.topRightGroupBox, 0, 1)
        mainLayout.addWidget(self.bottomLeftTabWidget, 1, 0, 1, 3)
        mainLayout.addWidget(self.bottomRightGroupBox, 2, 0, 1, 3)

        self.setLayout(mainLayout)

        self.setWindowTitle("Password generator")





    def createTopLeftGroupBox(self):
        self.topLeftGroupBox = QGroupBox("Group 1")

        uppercase_letters = QCheckBox("UPPERCASE letters")
        lowercase_letters = QCheckBox("lowercase letters")
        numbers = QCheckBox("Numbers 0-9")
        symbols = QCheckBox("Symbols !@#$%")
        togglePushButton = QPushButton("Default")
        uppercase_letters.setChecked(True)
        lowercase_letters.setChecked(True)
        numbers.setChecked(True)
        symbols.setChecked(True)

        layout = QVBoxLayout()
        layout.addWidget(uppercase_letters)
        layout.addWidget(lowercase_letters)
        layout.addWidget(numbers)
        layout.addWidget(symbols)
        layout.addWidget(togglePushButton)
        layout.addStretch(1)
        self.topLeftGroupBox.setLayout(layout)

    def createTopRightGroupBox(self):
        self.topRightGroupBox = QGroupBox("Group 2")

        title = QLabel("Password length")

        min_label = QLabel("Min length")
        max_label = QLabel("Min length")

        min_length = QSpinBox(self.topRightGroupBox)
        max_length = QSpinBox(self.topRightGroupBox)
        # lineEdit.setEchoMode(QLineEdit.Password)

        min_length.setValue(5)
        max_length.setValue(15)

        random_length = QCheckBox("RANDOME LENGTH")

        layout = QGridLayout()
        layout.addWidget(title, 0, 0)
        layout.addWidget(min_label, 1, 0)
        layout.addWidget(max_label, 2, 0)
        layout.addWidget(min_length, 1, 1)
        layout.addWidget(max_length, 2, 1)

        layout.addWidget(random_length, 3, 0, 1, 2)

        self.topRightGroupBox.setLayout(layout)


    def createBottomLeftTabWidget(self):
        self.bottomLeftTabWidget = QGroupBox("Generated password")

        lineEdit = QLineEdit('s3cRe7')

        layout = QVBoxLayout()

        layout.addWidget(lineEdit)
        self.bottomLeftTabWidget.setLayout(layout)



    def createBottomRightGroupBox(self):
        self.bottomRightGroupBox = QGroupBox()

        about_button = QPushButton("About")
        copy_button = QPushButton("Copy")
        generate_Button = QPushButton("Generate")

        layout = QGridLayout()

        layout.addWidget(about_button, 0, 1)
        layout.addWidget(copy_button, 0, 2)
        layout.addWidget(generate_Button, 0, 3)

        self.bottomRightGroupBox.setLayout(layout)


if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)
    gallery = WidgetGallery()
    gallery.show()
    sys.exit(app.exec_())